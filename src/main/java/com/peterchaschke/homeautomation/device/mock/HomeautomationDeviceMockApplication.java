package com.peterchaschke.homeautomation.device.mock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeautomationDeviceMockApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeautomationDeviceMockApplication.class, args);
	}

}
