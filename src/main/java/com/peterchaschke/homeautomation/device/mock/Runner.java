package com.peterchaschke.homeautomation.device.mock;

import javax.annotation.PreDestroy;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Runner implements CommandLineRunner {
	
	@Override
    public void run(String... args) throws Exception {
        main(args);
    }

    public static void main(String[] args) {
    	
    	System.out.println("Program running");
    	
    	SwitchClient switchClient = new SwitchClient("switch_1", "tcp://localhost:1883", "mock/switch");
    	switchClient.subscribe();
    	
    	SwitchClient switchClient2 = new SwitchClient("switch_2", "tcp://localhost:1883", "mock/switch2");
    	switchClient2.subscribe();
    	
    	SensorClient sensorClient = new SensorClient("sensor_1", "tcp://localhost:1883", "mock/sensor");
    	sensorClient.subscribe();
    	
    	LEDControllerClient ledClient = new LEDControllerClient("led_1", "tcp://localhost:1883", "mock/led");
    	ledClient.subscribe();
    }
    
    @PreDestroy
    public void destroy() {
        System.out.println(
          "Callback triggered - @PreDestroy.");
    }
}
