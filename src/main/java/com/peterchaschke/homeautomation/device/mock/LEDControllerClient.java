package com.peterchaschke.homeautomation.device.mock;

import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LEDControllerClient extends BaseClient implements MqttCallbackExtended {
	
	private static final Logger logger = LoggerFactory.getLogger(LEDControllerClient.class);
	
	protected final String RGB_KEY = "rgb";
	protected final String R_KEY = "r";
	protected final String G_KEY = "g";
	protected final String B_KEY = "b";
	protected final String TUN_KEY = "tun";
	protected final String WW_KEY = "ww";
	protected final String CW_KEY = "cw";
	protected final String BRIGHTNESS_KEY = "br";
	protected final String TIMER_ACTIVE_KEY = "active";
	protected final String ERR_KEY = "err";
	
	protected final String TIMER_END_RGB_EVENT = "TIMER_END_RGB";
	protected final String TIMER_END_TUNABLE_EVENT = "TIMER_END_TUNABLE";
	protected final String TIMER_END_WW_EVENT = "TIMER_END_WW";
	protected final String TIMER_END_CW_EVENT = "TIMER_END_CW";
	
	private LEDControllerChannelStatus rgbChannelStatus = new LEDControllerChannelStatus();
	private LEDControllerChannelStatus tunableChannelStatus = new LEDControllerChannelStatus();
	private LEDControllerChannelStatus wwChannelStatus = new LEDControllerChannelStatus();
	private LEDControllerChannelStatus cwChannelStatus = new LEDControllerChannelStatus();
	
	MqttClient client;
	IMqttClient publisher;
	

	public LEDControllerClient(String deviceName, String broker, String topic) {
		super(deviceName, broker, topic);
	}
	
	public void subscribe() {

		String clientId = UUID.randomUUID().toString();

		try {

			MqttClientPersistence persistence = new MemoryPersistence();
			client = new MqttClient(broker, clientId, persistence);

			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setAutomaticReconnect(true);
			connOpts.setCleanSession(false);
			connOpts.setKeepAliveInterval(30);

			client.connect(connOpts);
			client.setCallback(this);
			client.subscribe(topic, 1);

		} catch (IllegalArgumentException e) {

			throw new RuntimeException("Given broker URI is invalid.");

		} catch (MqttException e) {

			e.printStackTrace();
		}
	}
	
	@Override
	public void connectionLost(Throwable cause) {
		
		logger.info(String.format("Connection lost in switch client: %s", this.deviceName));
		
		subscribe();
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {

		logger.info(String.format("Message arrived in switch client: %s: %s", this.deviceName, message.toString()));
		
		try {
			JSONObject messageJson = new JSONObject(message.toString());
			
			if(!messageJson.has(MSG_ID_KEY)) {
				logger.info(String.format("No msgId present: %s", this.deviceName));
				return;
			}
			int messageId = messageJson.getInt(MSG_ID_KEY);
			logger.info(String.format("Message id: %d", messageId));
			
			if(messageJson.has(TOGGLE_ON_KEY)) {
				
				handleToggleOnRequest(messageJson.getJSONObject(TOGGLE_ON_KEY), messageId);

				return;
			}
			
			if(messageJson.has(TOGGLE_OFF_KEY)) {
				
				handleToggleOffRequest(messageJson.getJSONArray(TOGGLE_OFF_KEY), messageId);

				return;
			}
			
			if(messageJson.has(STATUS_KEY)) {
				
				handleStatusRequest(messageJson.getJSONArray(STATUS_KEY), messageId);
				
				return;
			}
			
			if(messageJson.has(EMULATE_KEY)) {
				
				handleEmulateRequest(messageJson.get(EMULATE_KEY).toString());
				
				return;
			}
			
			logger.error(String.format("No valid actions in request: %s", this.deviceName));
			
		} catch(JSONException e) {
			
			logger.error(String.format("Failed to deserialize message payload: %s", this.deviceName));
		}
		
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectComplete(boolean reconnect, String serverURI) {
		
		// Re-subscribe to the topic upon reconnect
		subscribe();
		
	}
	
	private void handleToggleOnRequest(JSONObject toggleJSON, int msgId) {
		
		logger.info(String.format("Toggle on request: %s: %s", this.deviceName, toggleJSON.toString()));
		
		boolean validParameters = false;
		
		JSONObject response = new JSONObject();
		response.put(MSG_RES_ID_KEY, String.valueOf(msgId));
		
		
		if(toggleJSON.has(RGB_KEY)) {

			JSONObject rgbRequest = toggleJSON.getJSONObject(RGB_KEY);
			
			if(!rgbRequest.has(R_KEY) || !rgbRequest.has(G_KEY) || !rgbRequest.has(B_KEY)) {
				logger.error(String.format("Missing required r, g, b values for toggle on: %s", this.deviceName));
				return;
			}
			
			if(!rgbRequest.has(BRIGHTNESS_KEY)) {
				// TODO: This can be a warning in the future when the device code itself is updated.
				// If there is no brightness provided, 100% will be given.
				logger.error(String.format("No brightness key specified for toggle on rgb: %s", this.deviceName));
				return;
			}
			int brightness = scaleBrightness(rgbRequest.getInt(BRIGHTNESS_KEY));
			this.rgbChannelStatus.setBrightness(brightness);
			
			this.rgbChannelStatus.setR(rgbRequest.getInt(R_KEY));
			this.rgbChannelStatus.setG(rgbRequest.getInt(G_KEY));
			this.rgbChannelStatus.setB(rgbRequest.getInt(B_KEY));
		    
		    if(rgbRequest.has(TIMER_KEY)) {
		    	int timer = rgbRequest.getInt(TIMER_KEY)*1000;
		    	this.rgbChannelStatus.setTimer(timer);
		    } else {
		    	this.rgbChannelStatus.setTimer(0);
		    }
		    
		    // "Turn on"
		    if(this.rgbChannelStatus.getTimer() > 0) {
		    	this.rgbChannelStatus.setTimerActive(true);
		    	this.rgbChannelStatus.setStart(System.currentTimeMillis());
		    }
		    this.rgbChannelStatus.setActive(1);
		    
		    JSONObject rgbResponse = new JSONObject();
		    rgbResponse.put(RGB_KEY, 1);
		    
		    response.put(TOGGLE_ON_KEY, rgbResponse);
		    
		    validParameters = true;
		    
		}
		
		if(toggleJSON.has(TUN_KEY)) {
			
			JSONObject tunRequest = toggleJSON.getJSONObject(TUN_KEY);
			
			if(!tunRequest.has(BRIGHTNESS_KEY)) {
				// TODO: This can be a warning in the future when the device code itself is updated.
				// If there is no brightness provided, 100% will be given.
				logger.error(String.format("No brightness key specified for toggle on tunable: %s", this.deviceName));
				return;
			}
			
			int brightness = tunRequest.getInt(BRIGHTNESS_KEY);
			
			if (brightness < 0) brightness = 0;
		    if (brightness > 100) brightness = 100;
		    
		    if(!tunRequest.has(WW_KEY)) {
				logger.error(String.format("No ww value specified for toggle on tunable: %s", this.deviceName));
				return;
			}
		    if(!tunRequest.has(CW_KEY)) {
				logger.error(String.format("No cw value specified for toggle on tunable: %s", this.deviceName));
				return;
			}

		    int wwVal = tunRequest.getInt(WW_KEY);
		    int cwVal = tunRequest.getInt(CW_KEY);
		    
		    float scale = (float) (brightness * .01);
		    this.wwChannelStatus.setBrightness((int) (wwVal * scale));
		    this.cwChannelStatus.setBrightness((int) (cwVal * scale));
		    this.tunableChannelStatus.setBrightness(brightness);
		    
		    // Keep from crashing board
		    if(wwChannelStatus.getBrightness() > 190 && cwChannelStatus.getBrightness() > 190) {
		    	
		    	JSONObject tunErrResponse = new JSONObject();
		    	tunErrResponse.put(TUN_KEY, 0);
		    	tunErrResponse.put(ERR_KEY, "WW/CW channel both exceed 75% bright");
			    
			    response.put(TOGGLE_ON_KEY, tunErrResponse);
			    MqttMessage message = new MqttMessage(response.toString().getBytes());
				
				try {
					logger.info(String.format("Sending toggle on response: %s: %s", this.deviceName, response.toString()));
					this.client.publish(this.topic, message);
					
				} catch (MqttException e) {
					
					logger.error(String.format("Failed to send toggle on response messsage: %s: %s", this.deviceName, e.getMessage()));
				}
				
				return;
		    }
		    
		    if(tunRequest.has(TIMER_KEY)) {
		    	int timer = tunRequest.getInt(TIMER_KEY)*1000;
		    	this.tunableChannelStatus.setTimer(timer);
		    } else {
		    	this.tunableChannelStatus.setTimer(0);
		    }
		    
		    this.wwChannelStatus.setActive(0);
		    this.wwChannelStatus.setTimerActive(false);
		    this.wwChannelStatus.setTimer(0);
		    this.cwChannelStatus.setActive(0);
		    this.cwChannelStatus.setTimerActive(false);
		    this.cwChannelStatus.setTimer(0);
			
		    // "Turn on"
		    if(this.tunableChannelStatus.getTimer() > 0) {
		    	this.tunableChannelStatus.setTimerActive(true);
		    	this.tunableChannelStatus.setStart(System.currentTimeMillis());
		    }
		    this.tunableChannelStatus.setActive(1);
		    
		    JSONObject tunResponse = new JSONObject();
		    tunResponse.put(TUN_KEY, 1);
		    
		    response.put(TOGGLE_ON_KEY, tunResponse);
		    
		    validParameters = true;
			
		}
		
		if(toggleJSON.has(WW_KEY)) {
			
			JSONObject wwRequest = toggleJSON.getJSONObject(WW_KEY);
			
			if(!wwRequest.has(BRIGHTNESS_KEY)) {
				// TODO: This can be a warning in the future when the device code itself is updated.
				// If there is no brightness provided, 100% will be given.
				logger.error(String.format("No brightness key specified for toggle on ww: %s", this.deviceName));
				return;
			}
			int brightness = scaleBrightness(wwRequest.getInt(BRIGHTNESS_KEY));
			this.wwChannelStatus.setBrightness(brightness);
			
			// Cancel tunable active
			this.tunableChannelStatus.setActive(0);
			
			// Keep from crashing board, turn off other white channel
		    if(this.wwChannelStatus.getBrightness() > 190 && this.cwChannelStatus.getBrightness() > 190) {
		      this.cwChannelStatus.setActive(0);
		    }
		    
		    if(wwRequest.has(TIMER_KEY)) {
		    	int timer = wwRequest.getInt(TIMER_KEY)*1000;
		    	this.wwChannelStatus.setTimer(timer);
		    } else {
		    	this.wwChannelStatus.setTimer(0);
		    }
			
		    // "Turn on" CW
		    if(this.wwChannelStatus.getTimer() > 0) {
		    	this.wwChannelStatus.setTimerActive(true);
		    	this.wwChannelStatus.setStart(System.currentTimeMillis());
		    }
		    
		    this.wwChannelStatus.setActive(1);
		    
		    JSONObject wwResponse = new JSONObject();
		    wwResponse.put(WW_KEY, 1);
		    
		    response.put(TOGGLE_ON_KEY, wwResponse);
		    
		    validParameters = true;
					
		}
		
		if(toggleJSON.has(CW_KEY)) {
			
			JSONObject cwRequest = toggleJSON.getJSONObject(CW_KEY);
			
			if(!cwRequest.has(BRIGHTNESS_KEY)) {
				// TODO: This can be a warning in the future when the device code itself is updated.
				// If there is no brightness provided, 100% will be given.
				logger.error(String.format("No brightness key specified for toggle on cw: %s", this.deviceName));
				return;
			}
			int brightness = scaleBrightness(cwRequest.getInt(BRIGHTNESS_KEY));
			this.cwChannelStatus.setBrightness(brightness);
			
			// Cancel tunable active
			this.tunableChannelStatus.setActive(0);
			
			// Keep from crashing board, turn off other white channel
		    if(this.wwChannelStatus.getBrightness() > 190 && this.cwChannelStatus.getBrightness() > 190) {
		      this.wwChannelStatus.setActive(0);
		    }
		    
		    if(cwRequest.has(TIMER_KEY)) {
		    	int timer = cwRequest.getInt(TIMER_KEY)*1000;
		    	this.cwChannelStatus.setTimer(timer);
		    } else {
		    	this.cwChannelStatus.setTimer(0);
		    }
			
		    // "Turn on" CW
		    if(this.cwChannelStatus.getTimer() > 0) {
		    	this.cwChannelStatus.setTimerActive(true);
		    	this.cwChannelStatus.setStart(System.currentTimeMillis());
		    }
		    
		    this.cwChannelStatus.setActive(1);
		    
		    JSONObject cwResponse = new JSONObject();
		    cwResponse.put(CW_KEY, 1);
		    
		    response.put(TOGGLE_ON_KEY, cwResponse);
		    
		    validParameters = true;
		}
		
		if(!validParameters) {
			logger.error(String.format("Invalid toggle parameters: %s", this.deviceName));
			return;
		}
		
		MqttMessage message = new MqttMessage(response.toString().getBytes());
		
		try {
			logger.info(String.format("Sending toggle on response: %s: %s", this.deviceName, response.toString()));
			this.client.publish(this.topic, message);
			
		} catch (MqttException e) {
			
			logger.error(String.format("Failed to send toggle on response messsage: %s: %s", this.deviceName, e.getMessage()));
		}
		
		return;
		
	}
	
	private int scaleBrightness(int inputBrightness) {

		  if (inputBrightness < 0) inputBrightness = 0;
		  if (inputBrightness > 100) inputBrightness = 100;

		  float scale = (float) (inputBrightness * .01);
		  int scaledBrightness = (int) (scale * 255);
		  return scaledBrightness;
	}
	
	private void handleToggleOffRequest(JSONArray toggleOffArray, int msgId) {
		
		logger.info(String.format("Toggle off request: %s: %s", this.deviceName, toggleOffArray.toString()));
		
		boolean validParameters = false;
		
		JSONObject response = new JSONObject();
		response.put(MSG_RES_ID_KEY, String.valueOf(msgId));
		
		this.rgbChannelStatus.setActive(0);
		this.rgbChannelStatus.setStart(0);
		this.rgbChannelStatus.setTimer(0);
		this.rgbChannelStatus.setTimerActive(false);
		
		this.tunableChannelStatus.setActive(0);
		this.tunableChannelStatus.setStart(0);
		this.tunableChannelStatus.setTimer(0);
		this.tunableChannelStatus.setTimerActive(false);
		
		this.wwChannelStatus.setActive(0);
		this.wwChannelStatus.setStart(0);
		this.wwChannelStatus.setTimer(0);
		this.wwChannelStatus.setTimerActive(false);
		
		this.cwChannelStatus.setActive(0);
		this.cwChannelStatus.setStart(0);
		this.cwChannelStatus.setTimer(0);
		this.cwChannelStatus.setTimerActive(false);
		
		
		if(toggleOffArray.toList().contains(RGB_KEY)) {
			
			JSONObject toggleOffResponse = new JSONObject();
			toggleOffResponse.put(RGB_KEY, 1);
		    
		    response.put(TOGGLE_OFF_KEY, toggleOffResponse);
		    
		    validParameters = true;
		}
		if(toggleOffArray.toList().contains(TUN_KEY)) {
			
			JSONObject toggleOffResponse = new JSONObject();
			toggleOffResponse.put(TUN_KEY, 1);
		    
		    response.put(TOGGLE_OFF_KEY, toggleOffResponse);
		    
		    validParameters = true;
		}
		if(toggleOffArray.toList().contains(WW_KEY)) {
					
			JSONObject toggleOffResponse = new JSONObject();
			toggleOffResponse.put(WW_KEY, 1);
		    
		    response.put(TOGGLE_OFF_KEY, toggleOffResponse);
		    
		    validParameters = true;
		}
		if(toggleOffArray.toList().contains(CW_KEY)) {
			
			JSONObject toggleOffResponse = new JSONObject();
			toggleOffResponse.put(CW_KEY, 1);
		    
		    response.put(TOGGLE_OFF_KEY, toggleOffResponse);
		    
		    validParameters = true;
		}
		
		
		if(!validParameters) {
			logger.error(String.format("Invalid toggle off parameters: %s", this.deviceName));
			return;
		}
		
		MqttMessage message = new MqttMessage(response.toString().getBytes());
		
		try {
			logger.info(String.format("Sending toggle off response: %s: %s", this.deviceName, response.toString()));
			this.client.publish(this.topic, message);
			
		} catch (MqttException e) {
			
			logger.error(String.format("Failed to send toggle off response messsage: %s: %s", this.deviceName, e.getMessage()));
		}
		
		return;
	}
	
	private void handleStatusRequest(JSONArray requestedStatusJSON, int msgId) {
		
		logger.info(String.format("Status request: %s: %s", this.deviceName, requestedStatusJSON.toString()));
		
		JSONObject response = new JSONObject();
		response.put(MSG_RES_ID_KEY, String.valueOf(msgId));
		
		System.out.println(requestedStatusJSON.toString());
		
		// TODO: Change this to be an array of status
		// Reference how switches work...
		JSONObject statusResponseObj = new JSONObject();
		
		requestedStatusJSON.forEach(status -> {
			
			if(status.toString().equalsIgnoreCase(PING_KEY)) {
				//JSONObject pingResponse = new JSONObject();
				//pingResponse.put(PING_KEY, "pong");
				statusResponseObj.put(PING_KEY, "pong");
			}
			
			if(status.toString().equalsIgnoreCase(STATE_KEY)) {
				JSONObject stateResponse = new JSONObject();
				
				
				stateResponse.put(RGB_KEY, this.rgbChannelStatus.getActive());
				stateResponse.put(TUN_KEY, this.tunableChannelStatus.getActive());
				stateResponse.put(WW_KEY, this.wwChannelStatus.getActive());
				stateResponse.put(CW_KEY, this.cwChannelStatus.getActive());
				
				statusResponseObj.put(STATE_KEY, stateResponse);
			}
			
			if(status.toString().equalsIgnoreCase(TIMERS_KEY)) {
				
				JSONObject timersResponse = new JSONObject();
				
				JSONObject rgbResponse = new JSONObject();
				if(this.rgbChannelStatus.isTimerActive()) {
					int remainingDelay = (int) ((this.rgbChannelStatus.getTimer() - (System.currentTimeMillis() - this.rgbChannelStatus.getStart())) / 1000);
					if(remainingDelay > 0) {
						rgbResponse.put(TIMER_ACTIVE_KEY, true);
						rgbResponse.put(TIMER_KEY, remainingDelay);
					} else {
						this.rgbChannelStatus.setTimer(0);
						this.rgbChannelStatus.setStart(0);
						this.rgbChannelStatus.setTimerActive(false);
						this.rgbChannelStatus.setActive(0);
						rgbResponse.put(TIMER_ACTIVE_KEY, false);
					}
					
				} else {
					
					rgbResponse.put(TIMER_ACTIVE_KEY, false);
				}
				timersResponse.put(RGB_KEY, rgbResponse);
				
				JSONObject tunResponse = new JSONObject();
				if(this.tunableChannelStatus.isTimerActive()) {
					int remainingDelay = (int) ((this.tunableChannelStatus.getTimer() - (System.currentTimeMillis() - this.tunableChannelStatus.getStart())) / 1000);
					if(remainingDelay > 0) {
						tunResponse.put(TIMER_ACTIVE_KEY, true);
						tunResponse.put(TIMER_KEY, remainingDelay);
					} else {
						this.tunableChannelStatus.setTimer(0);
						this.tunableChannelStatus.setStart(0);
						this.tunableChannelStatus.setTimerActive(false);
						this.tunableChannelStatus.setActive(0);
						tunResponse.put(TIMER_ACTIVE_KEY, false);
					}
					
				} else {
					this.tunableChannelStatus.setTimer(0);
					this.tunableChannelStatus.setStart(0);
					this.tunableChannelStatus.setTimerActive(false);
					tunResponse.put(TIMER_ACTIVE_KEY, false);
				}
				timersResponse.put(TUN_KEY, tunResponse);
				
				JSONObject wwResponse = new JSONObject();
				if(this.wwChannelStatus.isTimerActive()) {
					int remainingDelay = (int) ((this.wwChannelStatus.getTimer() - (System.currentTimeMillis() - this.wwChannelStatus.getStart())) / 1000);
					if(remainingDelay > 0) {
						wwResponse.put(TIMER_ACTIVE_KEY, true);
						wwResponse.put(TIMER_KEY, remainingDelay);
					} else {
						this.wwChannelStatus.setTimer(0);
						this.wwChannelStatus.setStart(0);
						this.wwChannelStatus.setTimerActive(false);
						wwResponse.put(TIMER_ACTIVE_KEY, false);
					}
					
				} else {
					this.wwChannelStatus.setTimer(0);
					this.wwChannelStatus.setStart(0);
					this.wwChannelStatus.setTimerActive(false);
					wwResponse.put(TIMER_ACTIVE_KEY, false);
				}
				timersResponse.put(WW_KEY, wwResponse);
				
				JSONObject cwResponse = new JSONObject();
				if(this.cwChannelStatus.isTimerActive()) {
					int remainingDelay = (int) ((this.cwChannelStatus.getTimer() - (System.currentTimeMillis() - this.cwChannelStatus.getStart())) / 1000);
					if(remainingDelay > 0) {
						cwResponse.put(TIMER_ACTIVE_KEY, true);
						cwResponse.put(TIMER_KEY, remainingDelay);
						this.cwChannelStatus.setActive(0);
					} else {
						this.cwChannelStatus.setTimer(0);
						this.cwChannelStatus.setStart(0);
						this.cwChannelStatus.setTimerActive(false);
						cwResponse.put(TIMER_ACTIVE_KEY, false);
					}
				} else {
					cwResponse.put(TIMER_ACTIVE_KEY, false);
				}
				timersResponse.put(CW_KEY, cwResponse);
				
				
				statusResponseObj.put(TIMERS_KEY, timersResponse);
			}
		});
		
		response.put(STATUS_KEY, statusResponseObj);
		
		MqttMessage message = new MqttMessage(response.toString().getBytes());
		
		try {
			logger.info(String.format("Sending status response: %s: %s", this.deviceName, response.toString()));
			this.client.publish(this.topic, message);
			
		} catch (MqttException e) {
			
			logger.error(String.format("Failed to send status response messsage: %s: %s", this.deviceName, e.getMessage()));
		}
		
		return;
	}
	
	private void handleEmulateRequest(String emulateRequest) {
		
		logger.info(String.format("Emulate request: %s: %s", this.deviceName, emulateRequest));
		
		boolean validRequest = false;
		JSONObject eventRequest = new JSONObject();
		
		if(emulateRequest.equalsIgnoreCase(TIMER_END_RGB_EVENT)) {
			
			eventRequest.put(DEVICE_NAME_KEY, this.deviceName);
			eventRequest.put(EVENT_KEY, TIMER_END_RGB_EVENT);
			
			validRequest = true;
		}
		if(emulateRequest.equalsIgnoreCase(TIMER_END_TUNABLE_EVENT)) {
					
			eventRequest.put(DEVICE_NAME_KEY, this.deviceName);
			eventRequest.put(EVENT_KEY, TIMER_END_TUNABLE_EVENT);
			
			validRequest = true;
		}
		if(emulateRequest.equalsIgnoreCase(TIMER_END_WW_EVENT)) {
			
			eventRequest.put(DEVICE_NAME_KEY, this.deviceName);
			eventRequest.put(EVENT_KEY, TIMER_END_WW_EVENT);
			
			validRequest = true;
		}
		if(emulateRequest.equalsIgnoreCase(TIMER_END_CW_EVENT)) {
			
			eventRequest.put(DEVICE_NAME_KEY, this.deviceName);
			eventRequest.put(EVENT_KEY, TIMER_END_CW_EVENT);
			
			validRequest = true;
		}
		
		if(validRequest) {
			MqttMessage message = new MqttMessage(eventRequest.toString().getBytes());
			
			try {
				logger.info(String.format("Sending timer end event: %s: %s", this.deviceName, eventRequest.toString()));
				this.client.publish(EVENT_TOPIC, message);
				
				return;
				
			} catch (MqttException e) {
				
				logger.error(String.format("Failed to send timer end event: %s: %s", this.deviceName, e.getMessage()));
			}
			
			return;
		}
		
		logger.error(String.format("Invalid emulate parameters: %s", this.deviceName));
	}

}
