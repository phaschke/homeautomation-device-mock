package com.peterchaschke.homeautomation.device.mock;

public class LEDControllerChannelStatus {
	
	private int active = 0; // 1 or 0
	private int r = 0; // 0 - 255
	private int g = 0; // 0 - 255
	private int b = 0; // 0 - 255
	private int brightness = 0; // 0 - 255 (input 0 - 100)
	private boolean timerActive = false; // true of false
	private long timer = 0; // length of timer in msecs
	private long start = 0;
	
	public int getActive() {
		return active;
	}
	
	public void setActive(int active) {
		this.active = active;
	}

	public int getR() {
		return r;
	}

	public void setR(int r) {
		this.r = r;
	}

	public int getG() {
		return g;
	}

	public void setG(int g) {
		this.g = g;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	public int getBrightness() {
		return brightness;
	}

	public void setBrightness(int brightness) {
		this.brightness = brightness;
	}

	public boolean isTimerActive() {
		return timerActive;
	}

	public void setTimerActive(boolean timerActive) {
		this.timerActive = timerActive;
	}

	public long getTimer() {
		return timer;
	}

	public void setTimer(long timer) {
		this.timer = timer;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}
}
