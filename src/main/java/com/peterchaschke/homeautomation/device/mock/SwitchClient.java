package com.peterchaschke.homeautomation.device.mock;

import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SwitchClient extends BaseClient implements MqttCallbackExtended {

	private static final Logger logger = LoggerFactory.getLogger(SwitchClient.class);
	
	int relayState = 0;
	
	long delayStart = 0; // the time the delay started
	long totalDelay;
	boolean delayRunning = false;
	
	MqttClient client;
	IMqttClient publisher;
	
	public SwitchClient(String deviceName, String broker, String topic) {
		super(deviceName, broker, topic);
	}
	
	public void subscribe() {

		String clientId = UUID.randomUUID().toString();

		try {

			MqttClientPersistence persistence = new MemoryPersistence();
			client = new MqttClient(broker, clientId, persistence);

			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setAutomaticReconnect(true);
			connOpts.setCleanSession(false);
			connOpts.setKeepAliveInterval(30);

			client.connect(connOpts);
			client.setCallback(this);
			client.subscribe(topic, 1);

		} catch (IllegalArgumentException e) {

			throw new RuntimeException("Given broker URI is invalid.");

		} catch (MqttException e) {

			e.printStackTrace();
		}
	}

	@Override
	public void connectionLost(Throwable cause) {
		
		logger.info(String.format("Connection lost in switch client: %s", this.deviceName));
		
		subscribe();
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {

		logger.info(String.format("Message arrived in switch client: %s: %s", this.deviceName, message.toString()));
		
		try {
			JSONObject messageJson = new JSONObject(message.toString());
			
			if(!messageJson.has(MSG_ID_KEY)) {
				logger.info(String.format("No msgId present: %s", this.deviceName));
				return;
			}
			int messageId = messageJson.getInt(MSG_ID_KEY);
			logger.info(String.format("Message id: %d", messageId));
			
			if(messageJson.has(TOGGLE_KEY)) {
				
				handleToggleRequest(messageJson.getJSONObject(TOGGLE_KEY), messageId);

				return;
			}
			
			if(messageJson.has(STATUS_KEY)) {
				
				handleStatusRequest(messageJson.getJSONArray(STATUS_KEY), messageId);
				
				return;
			}
			
			if(messageJson.has(EMULATE_KEY)) {
				
				handleEmulateRequest(messageJson.get(EMULATE_KEY).toString());
				
				return;
			}
			
			logger.error(String.format("No valid actions in request: %s", this.deviceName));
			
		} catch(JSONException e) {
			
			logger.error(String.format("Failed to deserialize message payload: %s", this.deviceName));
		}
		
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectComplete(boolean reconnect, String serverURI) {
		
		// Re-subscribe to the topic upon reconnect
		subscribe();
		
	}
	
	private void handleToggleRequest(JSONObject toggleJSON, int msgId) {
		
		logger.info(String.format("Toggle request: %s: %s", this.deviceName, toggleJSON.toString()));
		
		JSONObject response = new JSONObject();
		response.put(MSG_RES_ID_KEY, String.valueOf(msgId));
		
		if(toggleJSON.has(STATE_KEY)) {
			if(toggleJSON.getInt(STATE_KEY) == -1) {
				// Toggle off
				this.relayState = 0;
				this.delayStart = 0;
				this.totalDelay = 0;
				this.delayRunning = false;

				response.put(TOGGLE_SUCCESS_KEY, 1);
				
				MqttMessage message = new MqttMessage(response.toString().getBytes());
				
				try {
					logger.info(String.format("Sending toggle off response: %s: %s", this.deviceName, response.toString()));
					this.client.publish(this.topic, message);
					
				} catch (MqttException e) {
					
					logger.error(String.format("Failed to send toggle off response messsage: %s: %s", this.deviceName, e.getMessage()));
				}
				
				return;
			}
			if(toggleJSON.getInt(STATE_KEY) == 1) {
				// Toggle on
				
				this.relayState = 1;
				
				if(toggleJSON.has(DURATION_KEY)) {
					// Set timer
					this.delayStart = System.currentTimeMillis();
					this.delayRunning = true;
					this.totalDelay = toggleJSON.getInt(DURATION_KEY)*1000; // Duration is in seconds, convert to milliseconds
				}
				
				response.put(TOGGLE_SUCCESS_KEY, 1);
				
				MqttMessage message = new MqttMessage(response.toString().getBytes());
				
				try {
					logger.info(String.format("Sending toggle on response: %s: %s", this.deviceName, response.toString()));
					this.client.publish(this.topic, message);
					
				} catch (MqttException e) {
					
					logger.error(String.format("Failed to send toggle on response messsage: %s: %s", this.deviceName, e.getMessage()));
				}
				
				return;
			}
		}
		
		logger.error(String.format("Invalid toggle parameters: %s", this.deviceName));
	}
	
	private void handleStatusRequest(JSONArray requestedStatusJSON, int msgId) {
		
		logger.info(String.format("Status request: %s: %s", this.deviceName, requestedStatusJSON.toString()));
		
		JSONObject response = new JSONObject();
		response.put(MSG_RES_ID_KEY, String.valueOf(msgId));
		
		System.out.println(requestedStatusJSON.toString());
		
		JSONArray statusResponseArr = new JSONArray();
		
		requestedStatusJSON.forEach(status -> {
			
			if(status.toString().equalsIgnoreCase(PING_KEY)) {
				JSONObject pingResponse = new JSONObject();
				pingResponse.put(PING_KEY, "pong");
				statusResponseArr.put(pingResponse);
			}
			if(status.toString().equalsIgnoreCase(RELAY_KEY)) {
				JSONObject relayResponse = new JSONObject();
				relayResponse.put(RELAY_KEY, this.relayState);
				statusResponseArr.put(relayResponse);
			}
			if(status.toString().equalsIgnoreCase(TIMER_KEY)) {
				
				JSONObject timerResponse = new JSONObject();
				
				if (delayRunning && ((System.currentTimeMillis() - delayStart) <= totalDelay)) {
					int remainingDelay = (int) ((totalDelay - (System.currentTimeMillis() - delayStart))/1000);
					timerResponse.put(TIMER_KEY, remainingDelay);
				} else {
					timerResponse.put(TIMER_KEY, 0);
				}
				
				statusResponseArr.put(timerResponse);
			}
		});
		
		response.put(STATUS_KEY, statusResponseArr);
		
		MqttMessage message = new MqttMessage(response.toString().getBytes());
		
		try {
			logger.info(String.format("Sending status response: %s: %s", this.deviceName, response.toString()));
			this.client.publish(this.topic, message);
			
		} catch (MqttException e) {
			
			logger.error(String.format("Failed to send status response messsage: %s: %s", this.deviceName, e.getMessage()));
		}
		
		return;
	}
	
	private void handleEmulateRequest(String emulateRequest) {
		
		logger.info(String.format("Emulate request: %s: %s", this.deviceName, emulateRequest));
		
		// Emulate button press
		if(emulateRequest.equalsIgnoreCase(BUTTON_PRESS_EMULATE_KEY)) {
			
			JSONObject eventRequest = new JSONObject();
			eventRequest.put(DEVICE_NAME_KEY, this.deviceName);
			
			if(this.relayState == 0) {
				eventRequest.put(EVENT_KEY, BUTTON_PRESS_ON_KEY);
				
			} else {
				eventRequest.put(EVENT_KEY, BUTTON_PRESS_OFF_KEY);
			}
			
			MqttMessage message = new MqttMessage(eventRequest.toString().getBytes());
			
			try {
				logger.info(String.format("Sending button press event: %s: %s", this.deviceName, eventRequest.toString()));
				this.client.publish(EVENT_TOPIC, message);
				
				return;
				
			} catch (MqttException e) {
				
				logger.error(String.format("Failed to send button press event: %s: %s", this.deviceName, e.getMessage()));
			}
		}
		
		if(emulateRequest.equalsIgnoreCase(TIMER_END_EMULATE_KEY)) {
			
			JSONObject eventRequest = new JSONObject();
			eventRequest.put(DEVICE_NAME_KEY, this.deviceName);
			
			eventRequest.put(EVENT_KEY, TIMER_END_EVENT_KEY);
			
			MqttMessage message = new MqttMessage(eventRequest.toString().getBytes());
			
			try {
				logger.info(String.format("Sending timer end event: %s: %s", this.deviceName, eventRequest.toString()));
				this.client.publish(EVENT_TOPIC, message);
				
				this.relayState = 0;
				this.delayStart = 0;
				this.totalDelay = 0;
				this.delayRunning = false;
				
				return;
				
			} catch (MqttException e) {
				
				logger.error(String.format("Failed to send timer end event: %s: %s", this.deviceName, e.getMessage()));
			}
		}
		
		logger.error(String.format("Invalid emulate parameters: %s", this.deviceName));
	}

}
