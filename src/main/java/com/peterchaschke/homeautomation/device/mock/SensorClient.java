package com.peterchaschke.homeautomation.device.mock;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SensorClient extends BaseClient implements MqttCallbackExtended {

	private static final Logger logger = LoggerFactory.getLogger(SensorClient.class);
	
	protected final String TEMP_KEY = "temp";
	protected final String HUMID_KEY = "humid";
	protected final String LUX_KEY = "lux";
	
	MqttClient client;
	IMqttClient publisher;
	
	public SensorClient(String deviceName, String broker, String topic) {
		super(deviceName, broker, topic);
	}
	
	public void subscribe() {

		String clientId = UUID.randomUUID().toString();

		try {

			MqttClientPersistence persistence = new MemoryPersistence();
			client = new MqttClient(broker, clientId, persistence);

			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setAutomaticReconnect(true);
			connOpts.setCleanSession(false);
			connOpts.setKeepAliveInterval(30);

			client.connect(connOpts);
			client.setCallback(this);
			client.subscribe(topic, 1);

		} catch (IllegalArgumentException e) {

			throw new RuntimeException("Given broker URI is invalid.");

		} catch (MqttException e) {

			e.printStackTrace();
		}
	}
	
	@Override
	public void connectionLost(Throwable cause) {
		
		logger.info(String.format("Connection lost in status client: %s", this.deviceName));
		
		subscribe();
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {

		logger.info(String.format("Message arrived in status client: %s: %s", this.deviceName, message.toString()));
		
		try {
			JSONObject messageJson = new JSONObject(message.toString());
			
			if(!messageJson.has(MSG_ID_KEY)) {
				logger.info(String.format("No msgId present: %s", this.deviceName));
				return;
			}
			int messageId = messageJson.getInt(MSG_ID_KEY);
			logger.info(String.format("Message id: %d", messageId));
			
			if(messageJson.has(STATUS_KEY)) {
				
				handleStatusRequest(messageJson.getJSONArray(STATUS_KEY), messageId);
				
				return;
			}
			
			if(messageJson.has(EMULATE_KEY)) {
				
				handleEmulateRequest(messageJson.get(EMULATE_KEY).toString());
				
				return;
			}
			
			logger.error(String.format("No valid actions in request: %s", this.deviceName));
			
		} catch(JSONException e) {
			
			logger.error(String.format("Failed to deserialize message payload: %s", this.deviceName));
		}
		
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectComplete(boolean reconnect, String serverURI) {
		
		// Re-subscribe to the topic upon reconnect
		subscribe();
		
	}
	
	private void handleStatusRequest(JSONArray requestedStatusJSON, int msgId) {
		
		logger.info(String.format("Status request: %s: %s", this.deviceName, requestedStatusJSON.toString()));
		
		JSONObject response = new JSONObject();
		response.put(MSG_RES_ID_KEY, String.valueOf(msgId));
		
		System.out.println(requestedStatusJSON.toString());
		
		JSONArray statusResponseArr = new JSONArray();
		
		requestedStatusJSON.forEach(status -> {
			
			if(status.toString().equalsIgnoreCase(PING_KEY)) {
				JSONObject pingResponse = new JSONObject();
				pingResponse.put(PING_KEY, "pong");
				statusResponseArr.put(pingResponse);
			}
			
			if(status.toString().equalsIgnoreCase(TEMP_KEY)) {
				JSONObject tempResponse = new JSONObject();
				double randomDouble = generateRandomDouble(-50D, 120D, 1);
				tempResponse.put(TEMP_KEY, randomDouble);
				statusResponseArr.put(tempResponse);
			}
			
			if(status.toString().equalsIgnoreCase(HUMID_KEY)) {
				JSONObject humidResponse = new JSONObject();
				double randomDouble = generateRandomDouble(0D, 100D, 1);
				humidResponse.put(HUMID_KEY, randomDouble);
				statusResponseArr.put(humidResponse);
			}
			
			if(status.toString().equalsIgnoreCase(LUX_KEY)) {
				JSONObject luxResponse = new JSONObject();
				int randomInt = generateRandomInteger(0, 5000);
				luxResponse.put(LUX_KEY, randomInt);
				statusResponseArr.put(luxResponse);
			}

		});
		
		response.put(STATUS_KEY, statusResponseArr);
		
		MqttMessage message = new MqttMessage(response.toString().getBytes());
		
		try {
			logger.info(String.format("Sending status response: %s: %s", this.deviceName, response.toString()));
			this.client.publish(this.topic, message);
			
		} catch (MqttException e) {
			
			logger.error(String.format("Failed to send status response messsage: %s: %s", this.deviceName, e.getMessage()));
		}
		
		return;
	}
	
	private Double generateRandomDouble(double min, double max, int precision) {
		
		double generatedDouble = min + new Random().nextDouble() * (max - min);
		
		int scale = (int) Math.pow(10, precision);
	    return (double) Math.round(generatedDouble * scale) / scale;
	}
	
	private int generateRandomInteger(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}
	
	private void handleEmulateRequest(String emulateRequest) {
		
		logger.info(String.format("Emulate request: %s: %s", this.deviceName, emulateRequest));
		
		if(emulateRequest.equalsIgnoreCase(MOTION_SENSE_EMULATE_KEY)) {
			
			JSONObject eventRequest = new JSONObject();
			eventRequest.put(DEVICE_NAME_KEY, this.deviceName);
			
			eventRequest.put(EVENT_KEY, MOTION_SENSE_EVENT_KEY);
			
			MqttMessage message = new MqttMessage(eventRequest.toString().getBytes());
			
			try {
				logger.info(String.format("Sending motion sense event: %s: %s", this.deviceName, eventRequest.toString()));
				this.client.publish(EVENT_TOPIC, message);
				
				return;
				
			} catch (MqttException e) {
				
				logger.error(String.format("Failed to send motion sense event: %s: %s", this.deviceName, e.getMessage()));
			}
		}
		
		logger.error(String.format("Invalid emulate parameters: %s", this.deviceName));
	}

}
