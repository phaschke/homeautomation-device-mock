package com.peterchaschke.homeautomation.device.mock;

public class BaseClient {
	
	protected final String MSG_ID_KEY = "msgId";
	protected final String MSG_RES_ID_KEY = "resId";
	protected final String TOGGLE_KEY = "toggle";
	protected final String TOGGLE_ON_KEY = "toggle_on";
	protected final String TOGGLE_OFF_KEY = "toggle_off";
	protected final String STATUS_KEY = "status";
	protected final String EMULATE_KEY = "emulate";
	protected final String STATE_KEY = "state";
	protected final String DURATION_KEY = "duration";
	protected final String TOGGLE_SUCCESS_KEY = "toggleSuccess";
	protected final String PING_KEY = "ping";
	protected final String RELAY_KEY = "relay";
	protected final String TIMER_KEY = "timer";
	protected final String TIMERS_KEY = "timers";
	
	protected final String BUTTON_PRESS_EMULATE_KEY = "buttonPress";
	protected final String TIMER_END_EMULATE_KEY = "timerEnd";
	protected final String MOTION_SENSE_EMULATE_KEY = "motionSense";
	protected final String DEVICE_NAME_KEY = "name";
	protected final String EVENT_KEY = "event";
	protected final String TIMER_END_EVENT_KEY = "TIMER_END";
	protected final String BUTTON_PRESS_ON_KEY = "PRESS_ON";
	protected final String BUTTON_PRESS_OFF_KEY = "PRESS_OFF";
	protected final String MOTION_SENSE_EVENT_KEY = "MOTION";
	
	protected final String EVENT_TOPIC = "event/in";
	
	protected String deviceName;
	protected String broker;
	protected String topic;
	
	public BaseClient(String deviceName, String broker, String topic) {
		this.deviceName = deviceName;
		this.broker = broker;
		this.topic = topic;
	}
}
